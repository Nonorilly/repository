import admin_config
import telebot
import sqlite3
from telebot import types
vac_id = (0, )          # костыли костылики


class Database:
    def __init__(self, database):               # конструктор объекта бд (открытие), из config
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()

    def close(self):                            # закрытие бд
        self.connection.close()


bot = telebot.TeleBot(admin_config.token)             # токен бота из config


@bot.message_handler(commands=["start"])
def hello(message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1 = types.KeyboardButton("Да")
    keyboard.add(item1)
    bot.send_message(message.chat.id, "Приветствуем в HR-bot\nГотовы приступить к использованию?",
                     reply_markup=keyboard)


@bot.message_handler(content_types=["text"])
def step0(message):
    bot.send_message(message.chat.id, "Приступим.", reply_markup=types.ReplyKeyboardRemove())
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    db_worker = Database(admin_config.database_name)
    db_worker.cursor.execute("SELECT vacancy_name FROM vacancy_information")
    vac_name = db_worker.cursor.fetchall()
    item1 = types.KeyboardButton(vac_name[0][0])
    item2 = types.KeyboardButton(vac_name[1][0])
    item3 = types.KeyboardButton(vac_name[2][0])
    item4 = types.KeyboardButton(vac_name[3][0])
    item5 = types.KeyboardButton(vac_name[4][0])
    item6 = types.KeyboardButton(vac_name[5][0])
    item7 = types.KeyboardButton(vac_name[6][0])
    item8 = types.KeyboardButton(vac_name[7][0])
    item9 = types.KeyboardButton(vac_name[8][0])
    keyboard.add(item1, item2, item3, item4, item5, item6, item7, item8, item9)
    bot.send_message(message.chat.id, "Посмотреть кандидатов на вакансию: ", reply_markup=keyboard)
    bot.register_next_step_handler(message, step1)


def step1(message):
    db_worker = Database(admin_config.database_name)
    db_worker.cursor.execute("SELECT vacancy_id FROM vacancy_information WHERE vacancy_name = ?", [message.text])
    global vac_id
    vac_id = db_worker.cursor.fetchone()
    vac_name = message.text
    db_worker.cursor.execute("SELECT user_id FROM interview WHERE vacancy_id = ? AND status = ?", (vac_id[0], 5))
    users_vac = db_worker.cursor.fetchall()
    bot.send_message(message.chat.id, "Список кандидатов на вакансию " + vac_name + ":")
    if len(users_vac) == 0:
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        item1 = types.KeyboardButton("Вернуться к списку вакансий")
        keyboard.add(item1)
        bot.send_message(message.chat.id, "Список кандидатов пуст.", reply_markup=keyboard)
        bot.register_next_step_handler(message, step0)

    else:
        for i in range(len(users_vac)):
            db_worker.cursor.execute("SELECT answer FROM vacancy_answers WHERE user_id = ? AND question = ?",
                                     (users_vac[i][0], 'Ваше ФИО'))
            fio = db_worker.cursor.fetchone()
            bot.send_message(message.chat.id, str(i+1) + ". " + fio[0])     # много сообщений, можно переделать
        db_worker.close()
        bot.send_message(message.chat.id, "Выберете вакансию из списка для просмотра (номер): ",
                         reply_markup=types.ReplyKeyboardRemove())
        bot.register_next_step_handler(message, step2)


def step2(message):             # надеемся на то, что пока отвечали ситуация не поменялась
    s = message.text
    user_i = int(s)-1
    db_worker = Database(admin_config.database_name)
    db_worker.cursor.execute("SELECT user_id FROM interview WHERE vacancy_id = ? AND status = ?", (vac_id[0], 5))
    users_vac = db_worker.cursor.fetchall()
    if 0 <= user_i < len(users_vac):
        cur_user_id = users_vac[user_i]
        bot.send_message(message.chat.id, "[1] Резюме: ")
        db_worker.cursor.execute("SELECT resume_ref FROM interview WHERE user_id = ?", [cur_user_id[0]])
        ref = db_worker.cursor.fetchone()
        if ref[0] == '1':
            result = ""
            bot.send_message(message.chat.id, "Ссылки нет, проведен доп. опрос: ")
            db_worker.cursor.execute("SELECT * FROM resume_answers WHERE user_id = ?", [cur_user_id[0]])
            user_res = db_worker.cursor.fetchall()   # резюме очередного кандидата
            db_worker.close()
            for i in range(len(user_res)):
                result = result + str(user_res[i][1]) + " | -  " + str(user_res[i][2]) + "\n\n"   # формируем
            bot.send_message(message.chat.id, result)                        # большое сообщение с инфой
        else:
            bot.send_message(message.chat.id, "Ссылка на резюме кандидата: " + ref[0])  # можно ли сделать кликабельной
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        item1 = types.KeyboardButton("Да")
        item2 = types.KeyboardButton("Нет")
        keyboard.add(item1, item2)
        bot.send_message(message.chat.id, "Просмотреть результаты собеседования с этим кандидатом?: ",
                         reply_markup=keyboard)
        bot.register_next_step_handler(message, step3,  cur_user_id)
    else:
        bot.send_message(message.chat.id, "Кандидата с таким номером не существует. Введите новое значение")
        bot.register_next_step_handler(message, step2)


def step3(message, cur_user_id):
    db_worker = Database(admin_config.database_name)
    db_worker.cursor.execute("SELECT vacancy_name FROM vacancy_information WHERE vacancy_id = ?", [vac_id[0]])
    vac_name = db_worker.cursor.fetchone()
    if message.text == "Да":
        result = ""
        db_worker.cursor.execute("SELECT * FROM vacancy_answers WHERE user_id = ? AND vacancy_id = ?",
                                 (cur_user_id[0], vac_id[0]))
        user_res = db_worker.cursor.fetchall()  # резюме очередного кандидата
        bot.send_message(message.chat.id, "[2] Собеседование: ",
                         reply_markup=types.ReplyKeyboardRemove())
        for i in range(len(user_res)):
            result = result + str(user_res[i][2]) + " | -  " + str(user_res[i][3]) + "\n\n"  # формируем
        bot.send_message(message.chat.id, result)
        db_worker.cursor.execute("SELECT is_valid FROM interview WHERE user_id = ?", [cur_user_id[0]])
        user_is_valid = db_worker.cursor.fetchone()
        if user_is_valid[0]:
            bot.send_message(message.chat.id, "[Замечание] Кандидат под основные требования подходит ")
        else:
            bot.send_message(message.chat.id, "[Замечание] Кандидат под основные требования \bне подходит ")
        db_worker.cursor.execute("SELECT date FROM interview WHERE user_id = ?", [cur_user_id[0]])
        date = db_worker.cursor.fetchone()
        bot.send_message(message.chat.id, "Кандидат выбрал дату собеседования: " + date[0])

    elif message.text == "Нет":
        bot.send_message(message.chat.id, "Возвращаемся к выбору кандидатов на вакансию '" + vac_name[0] + "'",
                         reply_markup=types.ReplyKeyboardRemove())
        bot.send_message(message.chat.id, "Введите номер другого кандидата: ")
        bot.register_next_step_handler(message, step2)
    if message.text != "Нет":
        keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
        item1 = types.KeyboardButton("К списку вакансий")
        item2 = types.KeyboardButton(vac_name[0])
        keyboard.add(item1, item2)
        bot.send_message(message.chat.id, "Вернуться на этап выбора:", reply_markup=keyboard)
        bot.register_next_step_handler(message, step4)
    db_worker.close()


def step4(message):
    if message.text == "К списку вакансий":
        step0(message)
    else:
        step1(message)


# вечная мейн функция
if __name__ == '__main__':
    bot.infinity_polling()
