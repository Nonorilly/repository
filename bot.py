import config
import telebot
import sqlite3
from telebot import types
import datetime
import telegramcalendar

class Database:
    def __init__(self, database):               # конструктор объекта бд (открытие), из config
        self.connection = sqlite3.connect(database)
        self.cursor = self.connection.cursor()

    def close(self):                            # закрытие бд
        self.connection.close()


bot = telebot.TeleBot(config.token)             # токен бота из config
current_shown_dates={}

def check_status(user_id):                      # каждый этап собеседования - в виде числа поля status
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("SELECT * FROM interview WHERE user_id = ?;", [user_id])
    step = db_worker.cursor.fetchone()
    db_worker.close()
    if step is not None:
        return step[1]
    else:
        return -1


@bot.message_handler(commands=["start"])  # при команде /start пока что узнаем номер вакансии,
def init_start(message):                  # потом приделаем инфу о вакансиях и выбор оной не таким методом
    markup = types.ReplyKeyboardMarkup(resize_keyboard = True)
    item1 = types.KeyboardButton('Ссылка на сайт Parma TG')
    markup.add(item1)    
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("SELECT * FROM interview WHERE user_id = ?;", [message.from_user.id])
    user_add = db_worker.cursor.fetchone()     
    if user_add is None:
        user_add = (message.from_user.id, 0, '', '', '', 1, '')
        db_worker.cursor.execute("INSERT INTO interview VALUES(?, ?, ?, ?, ?, ?, ?);", user_add)            
        item2 = types.KeyboardButton('Информация о вакансиях')
        markup.add(item2) 
        bot.send_message(message.chat.id, "Хотите посмотреть вакансии?", reply_markup=markup)   
        db_worker.connection.commit()
        db_worker.close()
        #bot.send_message(message.chat.id, "Номер вакансии? 1/2")
        #bot.register_next_step_handler(message, init_vacancy_id)
    else:
        bot.send_message(message.chat.id, 'Ваша анкета уже была записана, для сброса введите: /reset')

@bot.callback_query_handler(func=lambda call: 'DAY' in call.data[0:13])
def handle_day_query(call):
    chat_id = call.message.chat.id
    saved_date = current_shown_dates.get(chat_id)
    last_sep = call.data.rfind(';') + 1

    if saved_date is not None:

        day = call.data[last_sep:]
        date = datetime.datetime(int(saved_date[0]), int(saved_date[1]), int(day), 0, 0, 0)       
        bot.send_message(chat_id=chat_id, text=str(date))        
        bot.answer_callback_query(call.id, text="")
        db_worker = Database(config.database_name)
        db_worker.cursor.execute("UPDATE interview SET date = ? WHERE user_id = ?;", (str(date), call.from_user.id))
        db_worker.connection.commit()
        db_worker.close()

    else:
        # add your reaction for shown an error
        pass


@bot.callback_query_handler(func=lambda call: 'MONTH' in call.data)
def handle_month_query(call):

    info = call.data.split(';')
    month_opt = info[0].split('-')[0]
    year, month = int(info[1]), int(info[2])
    chat_id = call.message.chat.id

    if month_opt == 'PREV':
        month -= 1

    elif month_opt == 'NEXT':
        month += 1

    if month < 1:
        month = 12
        year -= 1

    if month > 12:
        month = 1
        year += 1

    date = (year, month)
    current_shown_dates[chat_id] = date
    markup = telegramcalendar.create_calendar(year, month)
    bot.edit_message_text("Please, choose a date", call.from_user.id, call.message.message_id, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "IGNORE" in call.data)
def ignore(call):
    bot.answer_callback_query(call.id, text="OOPS... something went wrong")
@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if call.message:
        if call.data == "yes":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Отлично, прикрепите его в виде ссылки на документ")
            bot.register_next_step_handler(call.message, add_ref)
        elif call.data == "no":
            bot.edit_message_text(chat_id=call.message.chat.id, message_id=call.message.message_id,
                                  text="Для начала составим Ваше резюме")
            db_worker = Database(config.database_name)
            # обновляем pos на -1 для прохождения по вопросам-отсеивания, pos - итератор для вопросов
            db_worker.cursor.execute("UPDATE interview SET status = ?, pos = ?, resume_ref = ? WHERE user_id = ?",
                                     (2, -1, 1, call.from_user.id))
            db_worker.connection.commit()
            db_worker.close()
            bot.send_message(call.message.chat.id, "Начинаем?")  # вопрос склейка, ни на что не влияет

def add_ref(message):   # есть ссылка - добавляем, этап резюме игонрируем
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("UPDATE interview SET status = ?, pos = ?, resume_ref = ? WHERE user_id = ?",
                             (3, -1, message.text, message.from_user.id))
    db_worker.connection.commit()
    db_worker.close()
    bot.send_message(message.chat.id, "Замечательно. Можем перейти к этапу собеседования. Вы готовы?")

# при команде /reset сбрасываем анкету, удаляем запись из бд, оставлять ли эту функцию, если да, то
# дописать удаление ответов канидата из соответствующих таблиц
@bot.message_handler(commands=["reset"])
def reset_start(message):
    bot.send_message(message.chat.id, 'Анкета была успешно сброшена. Для создания новой - /start')
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("DELETE FROM interview WHERE user_id = ?", [message.from_user.id])
    db_worker.connection.commit()
    db_worker.close()


@bot.message_handler(func=lambda message: check_status(message.from_user.id) == 2)  # резюме в бой
def resume_qa(message):
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("SELECT * FROM resume_questions")
    quest = db_worker.cursor.fetchall()
    db_worker2 = Database(config.database_name)
    db_worker2.cursor.execute("SELECT pos FROM interview WHERE user_id = ?;", [message.from_user.id])
    pos = db_worker2.cursor.fetchone()
    i_pos = int(pos[0])
    if i_pos == -1:  # этот костыль нужен для игнорирования сообщения-ответа на вопрос пустышку для перехода в след этап
        i_pos += 1  # перешли в состояние вопросов
        bot.send_message(message.chat.id, quest[i_pos][0])
        i_pos += 1  # 1-ый вопрос задан, переходим к другим
        db_worker2.cursor.execute("UPDATE interview SET pos = ? WHERE user_id = ?", (i_pos, message.from_user.id))
        db_worker2.connection.commit()
    elif i_pos <= len(quest):
        if i_pos < len(quest):
            # print(i_pos, '  -  ', quest[i_pos][0])
            bot.send_message(message.chat.id, quest[i_pos][0])  # задаем свежий вопрос
        add_a = (message.from_user.id, quest[i_pos - 1][0], message.text)  # записываем ответ на предыдущий
        db_worker2.cursor.execute("INSERT INTO resume_answers VALUES(?, ?, ?);", add_a)
        db_worker2.connection.commit()
        i_pos += 1
        db_worker2.cursor.execute("UPDATE interview SET pos = ? WHERE user_id = ?", (i_pos, message.from_user.id))
        db_worker2.connection.commit()
    db_worker.close()
    db_worker2.close()
    if i_pos > len(quest):
        bot.send_message(message.chat.id, "Начнем собеседование?")
        db_worker = Database(config.database_name)
        db_worker.cursor.execute("UPDATE interview SET status = ?, pos = ? WHERE user_id = ?",
                                 (3, -1, message.from_user.id))
        db_worker.connection.commit()
        db_worker.close()

@bot.message_handler(func=lambda message: check_status(message.from_user.id) == 3)  # status 3,4 - собеседование
def qa_user(message):
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("SELECT vacancy_id FROM interview WHERE user_id = ?;", [message.from_user.id])
    vacancy_id = db_worker.cursor.fetchone()
    db_worker.cursor.execute("SELECT * FROM vacancy_questions WHERE vacancy_id = ?;", (vacancy_id[0],))
    quest = db_worker.cursor.fetchall()
    db_worker2 = Database(config.database_name)
    db_worker2.cursor.execute("SELECT pos FROM interview WHERE user_id = ?;", [message.from_user.id])
    pos = db_worker2.cursor.fetchone()
    i_pos = int(pos[0])
    if i_pos == -1:
        i_pos += 1
        bot.send_message(message.chat.id, quest[i_pos][1])
        i_pos += 1
        db_worker2.cursor.execute("UPDATE interview SET pos = ? WHERE user_id = ?", (i_pos, message.from_user.id))
        db_worker2.connection.commit()
    elif i_pos <= len(quest):
        if i_pos < len(quest):
            bot.send_message(message.chat.id, quest[i_pos][1])  # задаем свежий вопрос
        add_q = (message.from_user.id, vacancy_id[0], quest[i_pos-1][1], message.text)  # записываем ответ на предыдущий

        # топорная проверка ответа на соответствие требованиям
        if quest[i_pos-1][2] == "-interval":  # если необходимо проверить не просто совпадают ли значения, а например
            question = quest[i_pos-1][3]      # в случае необходимого опыта - он должен быть не менее
            l_quest = question.split(", ")
            is_good = False
            # print(l_quest)
            # print(message.text)
            # print(int(l_quest[0]) <= int(message.text))
            # for i in range(len(l_quest)):
            if int(l_quest[0]) <= int(message.text):
                is_good = True
            if is_good is False:
                # db_worker = Database(config.database_name)
                # если ответ нас не устроил - человек считается неподходящим
                db_worker.cursor.execute("UPDATE interview SET is_valid = ? WHERE user_id = ?", (0, message.from_user.id))
                db_worker.connection.commit()
                # db_worker.close()
        elif quest[i_pos-1][2] is not None:  # если есть эталонный ответ и нужно с ним сравнивать
            question = quest[i_pos - 1][2]
            l_quest = question.split(", ")
            is_good = False
            for i in range(len(l_quest)):
                if message.text == l_quest[i]:
                    is_good = True
            if is_good is False:
                # db_worker = Database(config.database_name)
                # если ответ нас не устроил - человек считается неподходящим
                db_worker.cursor.execute("UPDATE interview SET is_valid = ? WHERE user_id = ?", (0, message.from_user.id))
                db_worker.connection.commit()
                # db_worker.close()
        # конец

        # вне зависимости от того устроил ли ответ - записываем
        db_worker2.cursor.execute("INSERT INTO vacancy_answers VALUES(?, ?, ?, ?);", add_q)
        db_worker2.connection.commit()
        i_pos += 1
        db_worker2.cursor.execute("UPDATE interview SET pos = ? WHERE user_id = ?", (i_pos, message.from_user.id))
        db_worker2.connection.commit()
    db_worker.close()
    db_worker2.close()
    if i_pos > len(quest):
        # вопросы с разделением по вакансии кончились, обновляем счетчик вопросов для прохождения по общим
        db_worker = Database(config.database_name)
        db_worker.cursor.execute("UPDATE interview SET status = ?, pos = ? WHERE user_id = ?",
                                 (4, -1, message.from_user.id))
        db_worker.connection.commit()
        db_worker.close()
        bot.send_message(message.chat.id, "Готовы приступить ко второй части собеседования?")


# Вопросы без привязки к конкретной вакансии.
# Пока с кучей открытий закрытий соединений с бд для избежания проблем, возможна оптимизация (?)
@bot.message_handler(func=lambda message: check_status(message.from_user.id) == 4)  # общие вопросы
def qa_user2(message):
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("SELECT vacancy_id FROM interview WHERE user_id = ?;", [message.from_user.id])
    vacancy_id = db_worker.cursor.fetchone()
    db_worker.cursor.execute("SELECT * FROM vacancy_questions WHERE vacancy_id = ?;", [0])
    quest = db_worker.cursor.fetchall()
    db_worker2 = Database(config.database_name)
    db_worker2.cursor.execute("SELECT pos FROM interview WHERE user_id = ?;", [message.from_user.id])
    pos = db_worker2.cursor.fetchone()
    i_pos = int(pos[0])
    if i_pos == -1:
        i_pos += 1
        bot.send_message(message.chat.id, quest[i_pos][1])
        i_pos += 1
        db_worker2.cursor.execute("UPDATE interview SET pos = ? WHERE user_id = ?", (i_pos, message.from_user.id))
        db_worker2.connection.commit()
    elif i_pos <= len(quest):
        if i_pos < len(quest):
            bot.send_message(message.chat.id, quest[i_pos][1])  # задаем свежий вопрос
        add_q = (message.from_user.id, vacancy_id[0], quest[i_pos-1][1], message.text)  # записываем ответ на предыдущий
        # общие вопросы фильтров не имеют, добавляем в бд, но с привязкой к вакансии
        db_worker2.cursor.execute("INSERT INTO vacancy_answers VALUES(?, ?, ?, ?);", add_q)
        db_worker2.connection.commit()
        i_pos += 1
        db_worker2.cursor.execute("UPDATE interview SET pos = ? WHERE user_id = ?", (i_pos, message.from_user.id))
        db_worker2.connection.commit()
    db_worker.close()
    db_worker2.close()
    if i_pos > len(quest):
        # конец опроса для резюме, можно вводить новый статус и идти дальше
        # временная заглушка - просто переход к след. этапу который ничего не обрабатывает
        # статус 5 - конец (?)
        db_worker = Database(config.database_name)
        db_worker.cursor.execute("UPDATE interview SET status = ? WHERE user_id = ?", (5, message.from_user.id))
        db_worker.cursor.execute("SELECT is_valid FROM interview WHERE user_id = ?", [message.from_user.id])
        valid = db_worker.cursor.fetchone()  
        if(valid[0] == 1):
            now = datetime.datetime.now()
            chat_id = message.chat.id
            date = (now.year, now.month)
            current_shown_dates[chat_id] = date
            markup = telegramcalendar.create_calendar(now.year, now.month)
            bot.send_message(message.chat.id, "Пожалуйста, выберите дату собеседования", reply_markup=markup)   
        db_worker.connection.commit()
        db_worker.close()
        bot.send_message(message.chat.id, "Первичное собеседование окончено. Спасибо.")

@bot.message_handler(content_types=["text"])
def buttons(message):
    markup = types.ReplyKeyboardMarkup(resize_keyboard = True)
    db_worker = Database(config.database_name)
    db_worker.cursor.execute("SELECT vacancy_name FROM vacancy_information")
    vac_name = db_worker.cursor.fetchall()  
    item1 = types.KeyboardButton(vac_name[0][0])
    item2 = types.KeyboardButton(vac_name[1][0])
    item3 = types.KeyboardButton(vac_name[2][0])
    item4 = types.KeyboardButton(vac_name[3][0])
    item5 = types.KeyboardButton(vac_name[4][0])
    item6 = types.KeyboardButton(vac_name[5][0])
    item7 = types.KeyboardButton(vac_name[6][0])
    item8 = types.KeyboardButton(vac_name[7][0])
    item9 = types.KeyboardButton(vac_name[8][0])
    if message.text == "Ссылка на сайт Parma TG":
        keyboard = types.InlineKeyboardMarkup()
        url_button = types.InlineKeyboardButton(text="Сайт Parma Technologies", url="https://parma.ru/")
        keyboard.add(url_button)
        bot.send_message(message.chat.id, "Нажмите на кнопку, чтобы перейти на сайт компании", reply_markup=keyboard)
    elif message.text == "Информация о вакансиях":                    
        markup.add(item1,item2,item3,item4,item5,item6,item7,item8,item9) 
        bot.send_message(message.chat.id, "Доступные вакансии: ", reply_markup=markup) 
    elif message.text == "Выбрать эту вакансию":
        db_worker.cursor.execute("UPDATE interview SET status = ?, pos = ? WHERE user_id = ?",
                                 (1, -1, message.from_user.id))
        markup = types.ReplyKeyboardRemove()
        bot.send_message(message.chat.id, "Переходим на следующий этап...", reply_markup=markup)
        keyboard = types.InlineKeyboardMarkup()
        callback_button1 = types.InlineKeyboardButton(text="Да", callback_data="yes")
        keyboard.add(callback_button1)
        callback_button2 = types.InlineKeyboardButton(text="Нет", callback_data="no")
        keyboard.add(callback_button2)
        bot.send_message(message.chat.id, "У вас есть готовое резюме?", reply_markup=keyboard)
    for i in range (len(vac_name)):        
        if message.text == vac_name[i][0]:   
            db_worker.cursor.execute("SELECT vacancy_id FROM vacancy_information")
            vac_id = db_worker.cursor.fetchall()
            db_worker.cursor.execute("SELECT city FROM vacancy_information")
            vac_inf = db_worker.cursor.fetchall() 
            bot.send_message(message.chat.id, "Города с вакансией:", reply_markup=markup)
            bot.send_message(message.chat.id, vac_inf[i][0], reply_markup=markup)
            db_worker.cursor.execute("SELECT work_day FROM vacancy_information")
            vac_inf = db_worker.cursor.fetchall() 
            bot.send_message(message.chat.id, "Рабочий день:", reply_markup=markup)
            bot.send_message(message.chat.id, vac_inf[i][0], reply_markup=markup)
            db_worker.cursor.execute("SELECT work_experience FROM vacancy_information")
            vac_inf = db_worker.cursor.fetchall() 
            bot.send_message(message.chat.id, "Требуемый опыт работы:", reply_markup=markup)
            bot.send_message(message.chat.id, vac_inf[i][0], reply_markup=markup)
            db_worker.cursor.execute("SELECT requirements FROM vacancy_information")
            vac_inf = db_worker.cursor.fetchall() 
            bot.send_message(message.chat.id, "Требования к работнику:", reply_markup=markup)
            bot.send_message(message.chat.id, vac_inf[i][0], reply_markup=markup)
            db_worker.cursor.execute("SELECT opportunities FROM vacancy_information")
            vac_inf = db_worker.cursor.fetchall()
            bot.send_message(message.chat.id, "Предложения от компании:", reply_markup=markup)
            bot.send_message(message.chat.id, vac_inf[i][0], reply_markup=markup) 
            vac_choose = types.KeyboardButton("Выбрать эту вакансию")
            markup.add(item1,item2,item3,item4,item5,item6,item7,item8,item9, vac_choose) 
            bot.send_message(message.chat.id, "Хотите выбрать эту вакансию?", reply_markup=markup)   
            db_worker.cursor.execute("UPDATE interview SET vacancy_id = ? WHERE user_id = ?",
                                 (vac_id[i][0], message.from_user.id))
    db_worker.connection.commit()
    db_worker.close()


# вечная мейн функция
if __name__ == '__main__':
    bot.infinity_polling()
